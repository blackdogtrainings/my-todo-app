const INIT = {
    description: "",
    list: []
}

export default (state = INIT, action) => {
    switch(action.type) {
        case 'DESCRIPTION_CHANGE':
            return {...state, description: action.payload}
        case 'TODO_SEARCHED':
            return {...state, list: action.payload}    
        case 'TODO_ADDED':
            return {...state, description: ''}    
        case  'TODO_CLEAR':
            return {...state, description: ''}    
        default:
            return state    
    }
}